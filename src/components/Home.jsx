import React, { Component } from 'react';

class Home extends Component {

  render() {
    return(
      <div className="col-sm-4 col-sm-offset-4">
        <h1>My First Bootstrap Page</h1>
        <p>This part is inside a .container class.</p>
        <p>The .container class provides a responsive fixed width container.</p>
      </div>
    )
  }
}

export default Home;
